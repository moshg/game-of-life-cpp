#include <iostream>
#include <vector>

using std::vector;
using std::cout;
using std::endl;

struct Bool {
    bool value;

    Bool(bool value) : value(value) {}
};

class LifeGame {
    size_t width;
    size_t height;
    vector<Bool> is_lives;
    vector<uint8_t> counts_buf;

    uint8_t &count(size_t x, size_t y) {
        return counts_buf.at(x + y * width);
    }

    template <class OutT>
    void print_count(OutT out) {
        for (size_t i = 0; i < width + 1; ++i) {
            out << "--";
        }
        out << endl;

        for (size_t y = 0; y < height; ++y) {
            out << '|';
            for (size_t x = 0; x < width; ++x) {
                out << ' ' << (int) count(x, y);
            }
            out << '|' << endl;
        }

        for (size_t i = 0; i < width + 1; ++i) {
            out << "--";
        }
        out << endl;
    }

public:
    LifeGame(size_t width, size_t height) :
            width(width),
            height(height),
            is_lives(width * height, false),
            counts_buf(width * height, 0) {}

    bool &is_live(size_t x, size_t y) {
        return reinterpret_cast<bool &>(is_lives.at(x + y * width));
    }

    void update() {
        for (size_t y = 0; y < height; ++y) {
            for (size_t x = 0; x < width; ++x) {
                if (!is_live(x, y)) {
                    continue;
                }

                size_t min_x, max_x, min_y, max_y;
                if (x == 0) {
                    min_x = x;
                    max_x = x + 1;
                } else if (x == width - 1) {
                    min_x = x - 1;
                    max_x = x;
                } else {
                    min_x = x - 1;
                    max_x = x + 1;
                }

                if (y == 0) {
                    min_y = y;
                    max_y = y + 1;
                } else if (y == height - 1) {
                    min_y = y - 1;
                    max_y = y;
                } else {
                    min_y = y - 1;
                    max_y = y + 1;
                }

                for (size_t nbr_y = min_y; nbr_y <= max_y; ++nbr_y) {
                    for (size_t nbr_x = min_x; nbr_x <= max_x; ++nbr_x) {
                        if (!(nbr_x == x && nbr_y == y)) {
                            count(nbr_x, nbr_y) += 1;
                        }
                    }
                }
            }
        }

        for (size_t y = 0; y < height; ++y) {
            for (size_t x = 0; x < width; ++x) {
                auto cnt = count(x, y);
                if (is_live(x, y)) {
                    is_live(x, y) = cnt == 2 || cnt == 3;
                } else {
                    is_live(x, y) = cnt == 3;
                }
            }
        }

        for (auto &i: counts_buf) {
            i = 0;
        }
    }

    template <class T>
    void print(T &out) {
        for (size_t i = 0; i < width + 1; ++i) {
            out << "--";
        }
        out << endl;

        for (size_t y = 0; y < height; ++y) {
            out << '|';
            for (size_t x = 0; x < width; ++x) {
                if (is_live(x, y)) {
                    out << "[]";
                } else {
                    out << "  ";
                }
            }
            out << '|' << endl;
        }

        for (size_t i = 0; i < width + 1; ++i) {
            out << "--";
        }
        out << endl;
    }
};

int main() {
    auto game = LifeGame(5, 5);
    game.is_live(1, 0) = true;
    game.is_live(2, 1) = true;
    game.is_live(0, 2) = true;
    game.is_live(1, 2) = true;
    game.is_live(2, 2) = true;

    game.print(cout);
    cout << endl;

    for (auto i = 0; i < 5; ++i) {
        game.update();
        game.print(cout);
        cout << endl;
    }
}
